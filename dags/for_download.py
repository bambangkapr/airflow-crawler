import os
from datetime import datetime
from airflow.models import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

def download_from_s3(key: str, bucket_name: str, local_path: str) -> str:
  hook = S3Hook('s3_conn')
  file_name = hook.download_file(key=key, bucket_name=bucket_name, local_path=local_path)
  return file_name

def rename_file(ti, new_name: str) -> None:
  download_file_name = ti.xcom_pull(task_ids=['download_from_s3'])
  download_file_path = '/'.join(download_file_name[0].split('/')[:-1])
  os.rename(src=download_file_name[0], dst=f"{download_file_path}/{new_name}")

with DAG(
  dag_id='for_download',
  schedule_interval='@daily',
  start_date=datetime(2023, 10, 9),
  catchup=False
) as dag:

  task_download_from_s3 = PythonOperator(
    task_id='download_from_s3',
    python_callable=download_from_s3,
    op_kwargs={
      'key': 'posts.json',
      'bucket_name': 'bds-airflow-bucket',
      'local_path': 'Users/Enigma/Documents/Baba/CRAWLER AIRFLOW PYTHON/craw_airflow/dags/files_aduan'
      # 'local_path': 'Users/dradecic/airflow/data/'
    }
  )

  task_rename_file = PythonOperator(
    task_id='rename_file',
    python_callable=rename_file,
    op_kwargs={
      'new_name':'s3_download_post.json'
    }
  )