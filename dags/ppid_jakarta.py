# import database
import requests
from bs4 import BeautifulSoup
import json

def get_data():
    url = 'https://ppid.jakarta.go.id/informasi-berkala'
    r = requests.get(url)
    if r.status_code == 200:
      request = r.content
      soup = BeautifulSoup(request, 'html.parser')
      data = soup.body.section.findAll('a')

      title = soup.findAll('div', attrs={'class': 'subhurufawalgrey'})
      link = soup.findAll('a', attrs={'class': 'linklihatgrey'})

      count = 0
      for a in range(0, len(title)):
         count += 1
         print('{0}. {1}\n .{2}'.format(count, title[a].text.strip(), link[a].get('href')))
         data_json = json.dumps({
            "url":url,
            "title":title[a].text.strip(),
            "link":link[a].get('href')
         })

         # database.check_data(url, title[a].text.strip(), link[a].get('href'), data_json)