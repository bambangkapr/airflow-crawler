import requests
from bs4 import BeautifulSoup
from datetime import datetime
import json
import post_data

def get_data():
    url = 'https://ppid.jabarprov.go.id/page/568-Informasi-Berkala'
    r = requests.get(url)
    if r.status_code == 200:
        request = r.content
        soup = BeautifulSoup(request, 'html.parser')
        data = soup.body.section.findAll('a')

        count = 0
        data_list = []
        for a in range(0, len(data)):
            count += 1
            print('{0}. {1}\n .{2}'.format(count, data[a].text.strip(), data[a].get('href')))
            data_json = json.dumps({
                "url":url,
                "title":data[a].text.strip(),
                "link":data[a].get('href')
            })

            data_detail = {
                    "url": url,
                    "title": data[a].text.strip(),
                    "link": data[a].get('href'),
                    "json": data_json
                }

            data_list.append(data_detail)

        print('FETCH API')

        end_point = 'https://phinnisi.ilcs.co.id:3000/movement'
        # end_point = 'http://localhost:5000/products'
        header = {
        'Access_token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjo4ODksInVzZXJuYW1lIjoibDJhZG1zcmkiLCJlbWFpbCI6ImwyYWRtc3JpQGdtYWlsLmNvbSIsImlhdCI6MTY5NTAxMjgyN30.CFX6AXM21DN7xOdg88-Ftuz0B53aQFjO4Yaximd2JPM'
        }

        response = requests.get(url=end_point, headers=header)
        # response = requests.get(url=end_point)
        if response.status_code == 200:
            data = response.json()
            print(data, 'data')
        else:
            print(response.status_code, 'response.status_code')