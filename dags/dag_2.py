from airflow import DAG
# from airflow.operators.http_operator import HttpSensor
# from airflow.operators.python_operator import PythonOperator
# from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta
import requests

from airflow.operators.bash import BashOperator

# Define your DAG
default_args = {
    'owner': 'your_name',
    'start_date': datetime(2023, 10, 2),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    # 'download_file_dag',
    # default_args=default_args,
    # schedule_interval=None,  # Set your desired schedule interval
    # catchup=False,  # Set to True if you want to backfill historical data
    # is_paused_upon_creation=False,
    dag_id="dag_2_download_file", 
    start_date=datetime(2023, 10, 2), 
    schedule=timedelta(minutes=5),
)

# Define the URL of the file you want to download
file_url = 'https://opendata.karanganyarkab.go.id/dataset/e07335c4-b0c0-4b08-8f49-87089156de06/resource/6d3b88ca-f66c-47a9-b0b8-dab198c6d547/download/rekap-aduan-juli-2021.xlsx'

# Define a function to download the file
def download_file():
    response = requests.get(file_url)
    if response.status_code == 200:
        with open('/files_aduan', 'wb') as f:
            f.write(response.content)
    else:
        raise Exception(f"Failed to download file from {file_url}")

# Create a PythonOperator to download the file
download_task = BashOperator(
    task_id='download_file_task',
    python_callable=download_file,
    dag=dag,
)

# Set up task dependencies
# http_sensor >> download_task

if __name__ == "__main__":
    dag.cli()