import requests
from urllib.parse import urlparse
import os
import read_file
import json
# import fetch_api

def get_data():
  print('ADUAN')
  end_point = 'https://katalog.data.go.id/api/3/action/package_search?q=aduan&facet.field=%5B%22organization%22%2C%22kategori%22%2C%22prioritas_tahun%22%2C%22tags%22%2C%22res_format%22%5D&facet.limit=500&start=0&rows=20&include_private=true'
  data_aduan = []

  response = requests.get(url=end_point)
  if response.status_code == 200:
    data = response.json()
    results = data['result']['results']
    for result in results:
      for resource in result['resources']:
        if resource['format'] == 'XLSX':
          # print(f"https://katalog.data.go.id/dataset/{result['name']}")
          # print(result['name'], 'name')
          # print(result['notes'], 'notes')
          # print(result['organization']['title'], 'organization')
          # print(resource['url'], 'link download')

          url_download = resource['url']
          folder = '/files_aduan'
          nama_file = result['notes'] + ' ' + result['organization']['title'] + ".xlsx"
          print('nama_file', nama_file)

          # ambil file dari url_download
          # res = requests.get(url_download, allow_redirects=True)

          # # Menambahkan folder jika folder belum ada
          # if not os.path.exists(folder):
          #     os.makedirs(folder)

          # # lokasi folder dan file disimpan
          # path_file_lokal = os.path.join(folder, nama_file)

          # # ambil response request
          # if res.status_code == 200:              
          #   # jika tidak ada file dalam folder, maka tambahkan file
          #   if not os.path.exists(path_file_lokal):
          #       with open(path_file_lokal, 'wb') as file:
          #           print('SAVE FILE')
          #           file.write(res.content)
          
          # aduan_detail = read_file.f_read_file(folder, nama_file)

          aduan_header = {
             'end_point': end_point,
             'name': result['name'],
             'notes': result['notes'],
             'organization': result['organization']['title'],
             'url': resource['url'],
             'url_view': f"https://katalog.data.go.id/dataset/{result['name']}"
            #  'aduan_detail': aduan_detail
          }
          data_aduan.append(aduan_header)
    print('data_aduan', data_aduan)
    # fetch_api.simpan_aduan(data_aduan)
  else:
    print(response.status_code, 'response.status_code')