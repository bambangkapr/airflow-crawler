import requests
import json

def fetch_api(data):
  print('FETCH API')
  end_point = 'http://zakat.test/api/content'

  response = requests.post(url=end_point, json=data)
  if response.status_code == 200:
    data = response.json()
    print(data, 'data')
  else:
    print(response.status_code, 'response.status_code')