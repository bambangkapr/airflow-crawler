import requests
from bs4 import BeautifulSoup
import json

def get_data():
    url = 'https://ppid.kemenpppa.go.id/informasi/berkala'
    r = requests.get(url)
    request = r.content
    soup = BeautifulSoup(request, 'html.parser')
    data = soup.body.section.findAll('a')
    
    count = 0
    for a in range(0, len(data)):
        count += 1
        print('{0}. {1}\n .{2}'.format(count, data[a].text.strip(), data[a].get('href')))
        data_json = json.dumps({
            "url":url,
            "title":data[a].text.strip(),
            "link":data[a].get('href')
        })

        # database.check_data(url, data[a].text.strip(), data[a].get('href'), data_json)