import requests
from bs4 import BeautifulSoup
import json

def get_data():
    url = 'https://ppid.ombudsman.go.id/informasi/2'
    r = requests.get(url)
    request = r.content
    soup = BeautifulSoup(request, 'html.parser')
    data = soup.body.findAll('div', attrs={'class', 'wrap ms-hero-img-coffee ms-hero-bg-warning ms-bg-fixed'})[0].findAll('div', attrs={'class', 'list-group'})[0].findAll('div', attrs={'class', 'list-group-item list-group-item-action withripple'})

    count = 0
    for a in range(0, len(data)):
        count += 1
        td = data[a].table.findAll('td')
        link = td[1].find('a')

        if link:
            print('{0}. {1}\n .{2}'.format(count, td[0].text, link.get('href')))
            data_json = json.dumps({
                "url":url,
                "title":td[0].text,
                "link":link.get('href')
            })

            # database.check_data(url, td[0].text, link.get('href'), data_json)