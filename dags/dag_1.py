from datetime import datetime, timedelta

from airflow import DAG
from airflow.decorators import task
from airflow.operators.bash import BashOperator
import requests
import os
import pandas as pd

import ppid_jabarprov
import ppid_jakarta
import ppid_kemenppa
import ppid_kominfo
import ppid_ombudsman
import post_data
import aduan

# A DAG represents a workflow, a collection of tasks
with DAG(
    dag_id="craw_airflow", 
    start_date=datetime(2023, 10, 9), 
    schedule=timedelta(minutes=5),
    ) as dag:

    # Tasks are represented as operators
    op_ppid_jabarprov = BashOperator(task_id="ppid_jabarprov", bash_command="echo PPID JABARPROV")
    @task()
    def airflow_ppid_jabarprov():
        print("START AIRFLOW PPID JABARPROV")
        ppid_jabarprov.get_data()
        # post_data.fetch_api(result)
        print("FINISH AIRFLOW PPID JABARPROV")

    op_ppid_jakarta = BashOperator(task_id="ppid_jakarta", bash_command="echo PPID JAKARTA")
    @task()
    def airflow_ppid_jakarta():
        print("START AIRFLOW PPID JAKARTA")
        ppid_jakarta.get_data()
        print("FINISH AIRFLOW PPID JAKARTA")

    op_ppid_kemenppa = BashOperator(task_id="ppid_kemenppa", bash_command="echo PPID KEMENPPA")
    @task()
    def airflow_ppid_kemenppa():
        print("START AIRFLOW PPID KEMENPPA")
        ppid_kemenppa.get_data()
        print("FINISH AIRFLOW PPID KEMENPPA")

    op_ppid_kominfo = BashOperator(task_id="ppid_kominfo", bash_command="echo PPID KOMINFO")
    @task()
    def airflow_ppid_kominfo():
        print("START AIRFLOW PPID KOMINFO")
        ppid_kominfo.get_data()
        print("FINISH AIRFLOW PPID KOMINFO")

    op_ppid_ombudsman = BashOperator(task_id="ppid_ombudsman", bash_command="echo PPID OMBUDSMAN")
    @task()
    def airflow_ppid_ombudsman():
        print("START AIRFLOW PPID OMBUDSMAN")
        ppid_ombudsman.get_data()
        print("FINISH AIRFLOW PPID OMBUDSMAN")

    op_aduan = BashOperator(task_id="aduan", bash_command="echo ADUAN")
    @task()
    def airflow_aduan():
        print("START AIRFLOW ADUAN")
        # ppid_ombudsman.get_data()
        aduan.get_data()
        print("FINISH AIRFLOW ADUAN")

    op_download_file = BashOperator(task_id="download_file", bash_command="echo DOWNLOAD FILE")
    @task()
    def airflow_download_file():
        print("START AIRFLOW DOWNLOAD FILE")
        file_url = 'https://opendata.karanganyarkab.go.id/dataset/e07335c4-b0c0-4b08-8f49-87089156de06/resource/6d3b88ca-f66c-47a9-b0b8-dab198c6d547/download/rekap-aduan-juli-2021.xlsx'
        response = requests.get(file_url)


        if response.status_code == 200:
            save_folder = 'C:/Users/Enigma/Documents/Baba/CRAWLER AIRFLOW PYTHON/craw_airflow/dags/files_aduan'

            if not os.path.exists(save_folder):
                os.makedirs(save_folder)
            
            file_path = os.path.join(save_folder, 'file.xlsx')

            with open(file_path, 'wb') as f:
                print('SAVE FILE')
                f.write(response.content)
            
            df = pd.read_excel(file_path)
            # with open('/files_aduan', 'wb') as f:
            #     print('SAVE FILE')
            #     f.write(response.content)
        else:
            raise Exception(f"Failed to download file from {file_url}")
        print("FINISH AIRFLOW DOWNLOAD FILE")
        

    # Set dependencies between tasks
    op_ppid_jabarprov >> airflow_ppid_jabarprov()
    # op_ppid_jakarta >> airflow_ppid_jakarta()
    # op_ppid_kemenppa >> airflow_ppid_kemenppa()
    # op_ppid_kominfo >> airflow_ppid_kominfo()
    # op_ppid_ombudsman >> airflow_ppid_ombudsman()

    # op_aduan >> airflow_aduan()

    op_download_file >> airflow_download_file()