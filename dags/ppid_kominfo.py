import requests
from bs4 import BeautifulSoup
import json

def get_data():
    url = 'https://eppid.kominfo.go.id/informasi_publik/Informasi%20Publik%20Secara%20Berkala'
    r = requests.get(url)
    request = r.content
    soup = BeautifulSoup(request, 'html.parser')
    data = soup.body.findAll('div', attrs={'class', 'panel-group'})[0].findAll('div', attrs={'class', 'panel panel-default'})

    count = 0
    for a in range(0, len(data)):        
        tr = data[a].table.findAll('tr')

        for t in range(0, len(tr)):            
            if t == 0:
                continue
            
            td = tr[t].findAll('td')
            count += 1
            print('{0}. {1}\n .{2}'.format(count, td[1].text, td[3].find('a').get('href')))
            data_json = json.dumps({
                "url":url,
                "title":td[1].text,
                "link":td[3].find('a').get('href')
            })

            # database.check_data(url, td[1].text, td[3].find('a').get('href'), data_json)