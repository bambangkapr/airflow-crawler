import os
import pandas as pd

def f_read_file(folder, nama_file_excel):
  print('READ FILE')
  aduan_detail = []

  path_file_excel = os.path.join(folder, nama_file_excel)

  data = pd.read_excel(path_file_excel, skiprows=2)

  nama_kolom = 'ADUAN'

  if nama_kolom in data:
    for x in range(len(data[nama_kolom])):
      cek_kolom = isinstance(data[nama_kolom][x], str)
      if cek_kolom:
        # print('bukan nan')
        aduan = { 'aduan': data[nama_kolom][x] }
        aduan_detail.append(aduan)
  return aduan_detail